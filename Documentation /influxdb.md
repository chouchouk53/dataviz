Guide d'installation et d'utilisation de InfluxDB
=================================================

Dans ce guide, nous allons vous guider sur l'installation et l'utilisation de l'outil **InfluxDB**, que nous utiliserons dans le cadre de notre projet pour le **Golfe du Morbihan Vannes Agglomération** (GMVA).

**InfluxDB** sera l'outil commun entre les différents groupes travaillant sur ce projet qui nous permettra de stocker et gérer les différentes données que nous récupérerons grâce aux capteurs.

## Installation de InfluxDB avec Docker 

### Windows
* Pour vérifier que Docker est bien installé éxécuter la commande : docker -version
* Ensuite on installe influxdb sur docker en exécuter la commande :  docker run --name influxdb -p 8086:8086 quay.io/influxdb/influxdb:2.0.0-rc puis on va sur http://localhost:8086/

* Cliquer sur get started
* Configurer les paramètres d'accès : nom d'utilisateur, mot de passe, confirmer le mot de passe, donner un nom d'organisation et un nom de bucket
