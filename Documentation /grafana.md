Guide d'installation et d'utilisation de Grafana
================================================

Dans ce guide, nous allons vous guider sur l'installation et l'utilisation du logiciel **Grafana**, que nous utiliserons dans le cadre de notre projet pour le **Golfe du Morbihan Vannes Agglomération** (GMVA)

**Grafana** nous permettra d'exploiter les données stockés de la base InfluxDB en les visualisants avec des graphiques, des tableaux, et des indicateurs importants afin d'améliorer le suivi des installations communautaires, afin de diminuer de 30% les consommations du territoire à horizon 2030 par rapport à 2012.

## Installation de Grafana sur Docker 

### Windows

* Exécuter la commande : docker run -d -p 3000:3000 grafana/grafana-oss:9.2.5-ubuntu
* Se rendre sur votre navigueur préféré puis taper [localhost:3000](https://localhost:3000).
* Sur la page de login :
username :  admin 
password : admin 
* Bravo vous avez accédé à Graphana, maintenant rendez-vous sur l'onglet Token puis sur le bouton +generate et selectionner all access token.
* Pour y ajouter des données rendez vous sur configuration, data source, suivit de add data source
* Parmis les propositions dans Times series databases, ajouteé influxdb comme datasource
* aller dans settings : choisir flux comme query language puis remplir les details 
* 




